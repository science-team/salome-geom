// Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
//
// Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
// CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
//
// See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
//

//  GEOM OBJECT : interactive object for Geometry entities visualization
//  File   : GEOM_Actor.h
//  Author : Christophe ATTANASIO
//  Module : GEOM
//  $Header: /home/server/cvs/GEOM/GEOM_SRC/src/OBJECT/GEOM_Actor.h,v 1.11.2.3.6.8.2.2 2012-04-13 11:02:12 ana Exp $
//
#ifndef GEOM_ACTOR_H
#define GEOM_ACTOR_H

#include "GEOM_OBJECT_defs.hxx"
#include "GEOM_SmartPtr.h"

#include <SALOME_Actor.h>

#include <TopoDS_Shape.hxx>
#include <vtkSmartPointer.h>

class vtkCamera;

class GEOM_VertexSource;
typedef GEOM_SmartPtr<GEOM_VertexSource> PVertexSource;

class GEOM_EdgeSource;
typedef GEOM_SmartPtr<GEOM_EdgeSource> PEdgeSource;

class GEOM_WireframeFace;
typedef GEOM_SmartPtr<GEOM_WireframeFace> PWFaceSource;

class GEOM_ShadingFace;
typedef GEOM_SmartPtr<GEOM_ShadingFace> PSFaceSource;

class vtkRenderer;

class vtkAppendPolyData;
typedef GEOM_SmartPtr<vtkAppendPolyData> PAppendFilter;

class GEOM_OBJECT_EXPORT GEOM_Actor: public SALOME_Actor
{
public:
  vtkTypeMacro(GEOM_Actor,SALOME_Actor);
  static GEOM_Actor* New();

  void SetShape(const TopoDS_Shape& theShape,
		float theDeflection,
		bool theIsVector = false);

  void SetDeflection(float theDeflection);
  float GetDeflection() const{ return myDeflection;}

  void AddToRender(vtkRenderer* theRenderer);
  void RemoveFromRender(vtkRenderer* theRenderer);

  enum EDisplayMode{ eWireframe, eShading, eShadingWithEdges = eShading + 2 };

/*   void SetDisplayMode(EDisplayMode theMode);  */
/*   EDisplayMode GetDisplayMode() const { return myDisplayMode;}  */

  void SetSelected(bool theIsSelected);
  bool IsSelected() const { return myIsSelected;}

  // OLD METHODS
  // Properties
  void SetHighlightProperty(vtkProperty* Prop);
  void SetWireframeProperty(vtkProperty* Prop);
  void SetShadingProperty(vtkProperty* Prop);

  vtkProperty* GetWireframeProperty();
  vtkProperty* GetShadingProperty();

  void setDeflection(double adef);
  virtual void setDisplayMode(int thenewmode);

  // Description:
  // This causes the actor to be rendered. It, in turn, will render the actor's
  // property and then mapper.
  virtual void Render(vtkRenderer *, vtkMapper *);
  // Description:
  // Release any graphics resources that are being consumed by this actor.
  // The parameter window could be used to determine which graphic
  // resources to release.
  void ReleaseGraphicsResources(vtkWindow *);
  const TopoDS_Shape& getTopo();
  void setInputShape(const TopoDS_Shape& ashape, double adef1,
                     int imode, bool isVector = false);
  double getDeflection();
  double isVector();

  // SubShape
  void SubShapeOn();
  void SubShapeOff();

  // Highlight
  virtual void highlight(bool theHighlight);
  virtual bool hasHighlight() { return true; }

  void ShallowCopy(vtkProp *prop);

  // Opacity
  void SetOpacity(vtkFloatingPointType opa);
  vtkFloatingPointType GetOpacity();

  // Color
  void SetColor(vtkFloatingPointType r,vtkFloatingPointType g,vtkFloatingPointType b);
  void GetColor(vtkFloatingPointType& r,vtkFloatingPointType& g,vtkFloatingPointType& b);

  // Material
  void SetMaterial(std::vector<vtkProperty*> theProps);
  vtkProperty* GetMaterial();

  virtual bool IsInfinitive();

  // overloaded functions
  //! To map current selection to VTK representation
  virtual
  void
  Highlight(bool theHighlight);

  //----------------------------------------------------------------------------
  //! To process prehighlight (called from #SVTK_InteractorStyle)
  virtual
  bool
  PreHighlight(vtkInteractorStyle* theInteractorStyle,
               SVTK_SelectionEvent* theSelectionEvent,
               bool theIsHighlight);

  //! To process highlight (called from #SVTK_InteractorStyle)
  virtual
  bool
  Highlight(vtkInteractorStyle* theInteractorStyle,
            SVTK_SelectionEvent* theSelectionEvent,
            bool theIsHighlight);

  //! Visibility management
  virtual
  void
  SetVisibility( int );

  //! IsoLines management
  // theNb[0] - number of U lines
  // theNb[1] - number of V lines
  virtual
  void
  SetNbIsos(const int theNb[2]);

  virtual
  void
  GetNbIsos(int &theNbU,int &theNbV);
  
  virtual 
  void SetIsosWidth(const int width);

  int GetIsosWidth() const;

  virtual void SetWidth(const int width);

  int GetWidth() const;
  
  //! Vector mode management
  virtual
  void
  SetVectorMode(const bool theMode);

  virtual
  bool
  GetVectorMode();
  
  //! Edges in shading color management
  void SetEdgesInShadingColor(vtkFloatingPointType r,vtkFloatingPointType g,vtkFloatingPointType b);

  void
  StoreIsoNumbers();

  void
  RestoreIsoNumbers();
  
  void
  ResetIsoNumbers();

protected:
  void SetModified();

  void GetMatrix(vtkCamera* theCam, vtkMatrix4x4 *result);

  void StoreBoundaryColors();
  void RestoreBoundaryColors();

  GEOM_Actor();
  ~GEOM_Actor();

private:
  TopoDS_Shape myShape;
  int myNbIsos[2];
  bool isOnlyVertex;

  float myDeflection;
  bool myIsForced;

  //  EDisplayMode myDisplayMode;
  bool myIsSelected;
  bool myVectorMode;

  PDeviceActor myVertexActor;
  PVertexSource myVertexSource;

  PDeviceActor myIsolatedEdgeActor;
  PEdgeSource myIsolatedEdgeSource;

  PDeviceActor myOneFaceEdgeActor;
  PEdgeSource myOneFaceEdgeSource;

  PDeviceActor mySharedEdgeActor;
  PEdgeSource mySharedEdgeSource;

  PDeviceActor myWireframeFaceActor;
  PWFaceSource myWireframeFaceSource;

  PDeviceActor myShadingFaceActor;
  PSFaceSource myShadingFaceSource;

  PDeviceActor myHighlightActor;
  vtkSmartPointer<vtkProperty>  myHighlightProp;
  vtkSmartPointer<vtkProperty>  myPreHighlightProp;
  vtkSmartPointer<vtkProperty>  myShadingFaceProp;

  PAppendFilter myAppendFilter;
  PPolyGeomPainterDataMapper myPolyDataMapper;

  virtual void SetMapper(vtkMapper*);

  GEOM_Actor(const GEOM_Actor&);
  void operator=(const GEOM_Actor&);

  vtkFloatingPointType myEdgesInWireframeColor[3];
  vtkFloatingPointType myEdgesInShadingColor[3];
};

#endif //GEOM_ACTOR_H


