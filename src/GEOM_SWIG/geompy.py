#  -*- coding: iso-8859-1 -*-
# Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
#
# Copyright (C) 2003-2007  OPEN CASCADE, EADS/CCR, LIP6, CEA/DEN,
# CEDRAT, EDF R&D, LEG, PRINCIPIA R&D, BUREAU VERITAS
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

#  GEOM GEOM_SWIG : binding of C++ omplementaion with Python
#  File   : geompy.py
#  Author : Paul RASCLE, EDF
#  Module : GEOM
#  $Header: /home/server/cvs/GEOM/GEOM_SRC/src/GEOM_SWIG/geompy.py,v 1.26.2.3.6.3.14.1 2012-04-13 05:48:08 vsr Exp $
#
import salome
import geompyDC
from salome import *

geom = lcc.FindOrLoadComponent("FactoryServer", "GEOM")
geom.init_geom(salome.myStudy)

# Export the methods of geompyDC
for k in dir(geom):
  if k[0] == '_':continue
  globals()[k]=getattr(geom,k)
del k
from geompyDC import ShapeType, GEOM, kind, info, PackData, ReadTexture, EnumToLong

