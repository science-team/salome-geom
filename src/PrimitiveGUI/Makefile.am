# Copyright (C) 2007-2012  CEA/DEN, EDF R&D, OPEN CASCADE
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
#
# See http://www.salome-platform.org/ or email : webmaster.salome@opencascade.com
#

# File    : Makefile.am
# Author  : Alexander BORODIN, Open CASCADE S.A.S. (alexander.borodin@opencascade.com)
# Package : PrimitiveGUI
#
include $(top_srcdir)/adm_local/unix/make_common_starter.am

# header files 
salomeinclude_HEADERS =			\
	PrimitiveGUI.h			\
	PrimitiveGUI_BoxDlg.h		\
	PrimitiveGUI_CylinderDlg.h	\
	PrimitiveGUI_SphereDlg.h	\
	PrimitiveGUI_TorusDlg.h		\
	PrimitiveGUI_ConeDlg.h		

# Libraries targets
lib_LTLIBRARIES = libPrimitiveGUI.la

dist_libPrimitiveGUI_la_SOURCES =	\
	PrimitiveGUI.h			\
	PrimitiveGUI_BoxDlg.h		\
	PrimitiveGUI_ConeDlg.h		\
	PrimitiveGUI_CylinderDlg.h	\
	PrimitiveGUI_SphereDlg.h	\
	PrimitiveGUI_TorusDlg.h		\
	PrimitiveGUI_FaceDlg.h          \
	PrimitiveGUI_DiskDlg.h          \
					\
	PrimitiveGUI.cxx		\
	PrimitiveGUI_BoxDlg.cxx		\
	PrimitiveGUI_CylinderDlg.cxx	\
	PrimitiveGUI_SphereDlg.cxx	\
	PrimitiveGUI_TorusDlg.cxx	\
	PrimitiveGUI_ConeDlg.cxx        \
	PrimitiveGUI_FaceDlg.cxx        \
	PrimitiveGUI_DiskDlg.cxx

MOC_FILES =					\
	PrimitiveGUI_BoxDlg_moc.cxx		\
	PrimitiveGUI_CylinderDlg_moc.cxx	\
	PrimitiveGUI_SphereDlg_moc.cxx		\
	PrimitiveGUI_TorusDlg_moc.cxx		\
	PrimitiveGUI_ConeDlg_moc.cxx            \
	PrimitiveGUI_FaceDlg_moc.cxx		\
	PrimitiveGUI_DiskDlg_moc.cxx

nodist_libPrimitiveGUI_la_SOURCES =	\
	$(MOC_FILES)

# additional information to compile and link file

libPrimitiveGUI_la_CPPFLAGS =			\
	$(QT_INCLUDES) 				\
	$(VTK_INCLUDES)				\
	$(CAS_CPPFLAGS)				\
	$(PYTHON_INCLUDES)			\
	$(BOOST_CPPFLAGS)			\
	$(KERNEL_CXXFLAGS)			\
	$(GUI_CXXFLAGS)				\
	$(CORBA_CXXFLAGS)			\
	$(CORBA_INCLUDES)			\
	-I$(srcdir)/../GEOMGUI			\
	-I$(srcdir)/../DlgRef			\
	-I$(srcdir)/../GEOMBase			\
	-I$(srcdir)/../OBJECT			\
	-I$(srcdir)/../GEOMClient		\
	-I$(srcdir)/../GEOMImpl			\
	-I$(srcdir)/../GEOMFiltersSelection	\
	-I$(top_builddir)/src/DlgRef		\
	-I$(top_builddir)/idl

libPrimitiveGUI_la_LDFLAGS = 					\
	../GEOMFiltersSelection/libGEOMFiltersSelection.la	\
	../GEOMBase/libGEOMBase.la
