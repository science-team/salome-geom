
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 Documentation of the programming interface (API)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This section describes the python packages and modules of the
``salome.geom`` python package. The main part is generated from the
code documentation included in source python files.

:mod:`salome.geom` -- Package containing the GEOM python utilities
==================================================================

:mod:`geomtools` -- Tools to access GEOM engine and objects
-----------------------------------------------------------

.. automodule:: salome.geom.geomtools
   :members:

:mod:`sketcher` -- Wrapper to help the creation of simple sketches
------------------------------------------------------------------

.. automodule:: salome.geom.sketcher
   :members:

.. autoclass:: Sketcher
   :members:

:mod:`structelem` -- Structural elements package
------------------------------------------------

.. automodule:: salome.geom.structelem

.. autoclass:: StructuralElementManager
   :members:

.. autoclass:: StructuralElement
   :members:

:mod:`structelem.parts` -- Structural element parts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: salome.geom.structelem.parts
   :members:
   :undoc-members:
   :show-inheritance:

:mod:`structelem.orientation` -- Structural element orientation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: salome.geom.structelem.orientation
   :members:
   :undoc-members:
